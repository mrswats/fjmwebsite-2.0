from django.urls import path

from deblog import views

urlpatterns = [
    path("", views.post_list, name="blog"),
    path("preview/<slug:slug>/", views.post_detail_preview, name="preview"),
    path("<int:year>/<int:month>/<slug:slug>/", views.post_detail, name="post"),
    path("<int:year>/", views.post_list_per_year, name="post-year"),
]

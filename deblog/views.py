from typing import Any

from django import shortcuts
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import QuerySet
from django.http import HttpRequest
from django.http import HttpResponse

from deblog import models


def _post_list_paginated_response(
    request: HttpRequest, queryset: QuerySet, extra_context: dict[str, Any] | None = None
) -> HttpResponse:
    pages = Paginator(queryset, settings.BLOG_PAGE_SIZE)
    page_number = request.GET.get("page", "1")
    context = {
        "posts": pages.get_page(page_number),
        "pagination": pages.get_elided_page_range(number=page_number, on_ends=1),
    }
    context.update(extra_context if extra_context is not None else {})
    return shortcuts.render(request, "blog/list.html", context)


def post_list(request: HttpRequest) -> HttpResponse:
    tag = request.GET.get("t", "")
    post_list = models.Post.objects.filter(published_date__isnull=False, _tags__contains=tag)
    years = (
        post_list.values_list(("published_date__year"), flat=True)
        .distinct()
        .order_by("published_date__year")
    )
    return _post_list_paginated_response(
        request, post_list, extra_context={"t": tag, "years": years}
    )


def post_list_per_year(request: HttpRequest, year: int) -> HttpResponse:
    post_list = models.Post.objects.filter(published_date__isnull=False, published_date__year=year)
    return _post_list_paginated_response(request, post_list, extra_context={"year_view": True})


def post_detail(request: HttpRequest, year: int, month: int, slug: str) -> HttpResponse:
    post = shortcuts.get_object_or_404(
        models.Post.objects.select_related("author"),
        published_date__year=year,
        published_date__month=month,
        slug=slug,
    )

    return shortcuts.render(request, "blog/post.html", {"post": post})


@login_required
def post_detail_preview(request: HttpRequest, slug: str) -> HttpResponse:
    post = shortcuts.get_object_or_404(models.Post, published_date__isnull=True, slug=slug)
    return shortcuts.render(request, "blog/post_preview.html", {"post": post})

from urllib.parse import urlencode

from django import template

register = template.Library()


@register.simple_tag
def querystring(*_, **kwargs: tuple[str, str]) -> str:
    query_string = {key: str(val).strip() for key, val in kwargs.items() if val}
    encoded_query_string = urlencode(query_string)
    return f"?{encoded_query_string}" if encoded_query_string else ""

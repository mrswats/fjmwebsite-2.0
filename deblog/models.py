import re

import markdown
from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.html import strip_tags
from django.utils.text import slugify

MAX_TITLE_LENGTH = 200


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=MAX_TITLE_LENGTH)
    text = models.TextField()
    html = models.TextField()
    _tags = models.CharField(max_length=120)
    slug = models.SlugField(max_length=MAX_TITLE_LENGTH, default="")
    created_date = models.DateField(default=timezone.now)
    published_date = models.DateField(blank=True, null=True)

    @property
    def tags(self) -> list[str]:
        return [tag.strip() for tag in self._tags.split(",")]

    @property
    def is_published(self) -> bool:
        return bool(self.published_date)

    def publish(self) -> None:
        self.published_date = timezone.now()
        self.save()

    def unpublish(self) -> None:
        self.published_date = None
        self.save()

    def save(self, *args, **kwargs) -> None:
        self.slug = slugify(self.title)
        html = markdown.markdown(
            self.text,
            output_format="html",
            extensions=settings.MARKDOWN_EXTENSIONS,
            extension_configs=settings.MARKDOWN_EXTENSIONS_CONFIG,
        )
        self.html = re.sub(r"\n\n", "\n", html)
        super().save(*args, **kwargs)

    def preview(self) -> str:
        first_paragraph = re.search(r"<p>(.+)</p>", self.html)
        return first_paragraph.group(0) if first_paragraph else ""

    def preview_text(self) -> str:
        return strip_tags(self.preview())

    def get_absolute_url(self) -> str:
        if self.is_published:
            url_name = "post"
            url_kwargs = {
                "year": self.published_date.year,
                "month": self.published_date.month,
                "slug": self.slug,
            }
        else:
            url_name = "preview"
            url_kwargs = {"slug": self.slug}

        return reverse(url_name, kwargs=url_kwargs)

    def __str__(self) -> str:
        return f"({self.created_date.strftime('%Y-%m-%d')}): {self.title}"

    class Meta:
        ordering = ["-published_date"]

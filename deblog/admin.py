from html import escape

from django.contrib import admin
from django.db.models import QuerySet
from django.http import HttpRequest
from django.utils.html import format_html

from deblog import models


@admin.action(description="Publish a post.")
def publish_post(modeladmin: admin.ModelAdmin, request: HttpRequest, queryset: QuerySet) -> None:
    for post in queryset:
        post.publish()


@admin.action(description="Unpublish a post.")
def unpublish_post(modeladmin, request: HttpRequest, queryset: QuerySet) -> None:
    for post in queryset:
        post.unpublish()


@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    actions = [publish_post, unpublish_post]
    date_hierarchy = "created_date"
    autocomplete_fields = ("author",)
    fields = (
        "created_date",
        "published_date",
        "author",
        "title",
        "_tags",
        "text",
    )

    list_display = (
        "title",
        "created_date",
        "is_published",
        "published_date",
        "tags",
        "view_post",
    )

    def view_post(self, obj):
        path = escape(obj.get_absolute_url())
        return format_html('<a href="{path}">{path}</a>', path=path)

import os

import uvicorn


def main() -> int:
    uvicorn.run(
        "fjmwebsite.asgi:application",
        host="0.0.0.0",
        port=int(os.getenv("PORT", "1031")),
        workers=1,
        lifespan="off",
        loop="asyncio",
        log_level="info",
        access_log=True,
        use_colors=True,
        proxy_headers=True,
    )

    return 0


if __name__ == "__main__":
    raise SystemExit(main())

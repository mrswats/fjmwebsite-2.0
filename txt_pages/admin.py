from django.contrib import admin
from django.http import HttpRequest

from txt_pages.models import TXTPage


@admin.register(TXTPage)
class TXTPageAdmin(admin.ModelAdmin):
    list_display = ("name", "url", "body")
    fields = ("name", "url", "body")

    def get_readonly_fields(
        self, request: HttpRequest, obj: TXTPage | None = None
    ) -> tuple[str, ...]:
        return ("name", "url") if obj else ()

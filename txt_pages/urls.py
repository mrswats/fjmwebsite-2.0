from django import urls

from txt_pages.views import render_txt

urlpatterns = [
    urls.re_path(r"(?P<page_name>\w+\.txt)$", render_txt, name="txt"),
]

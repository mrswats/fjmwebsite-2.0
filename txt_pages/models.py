from django.db import models


class TXTPage(models.Model):
    name = models.CharField(max_length=100)
    url = models.CharField(max_length=100, blank=True, default="")
    body = models.TextField()

    def __str__(self) -> str:
        return self.name

    class Meta:
        unique_together = [
            ("name", "url"),
        ]

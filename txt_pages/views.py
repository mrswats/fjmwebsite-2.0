from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from txt_pages.models import TXTPage

NEWLINE_CHARACTER = "\n"


def render_txt(request: HttpRequest, page_name: str) -> HttpResponse:
    page = get_object_or_404(TXTPage, url=page_name)
    txt = page.body if page.body[-1] == NEWLINE_CHARACTER else f"{page.body}\n"
    return HttpResponse(content=txt, content_type="text/plain")

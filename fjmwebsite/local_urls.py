from django import urls

from fjmwebsite.urls import urlpatterns as original_urls

urlpatterns = [
    *original_urls,
    urls.path("__reload__/", urls.include("django_browser_reload.urls")),
]

from django import urls
from django.contrib import admin

urlpatterns = [
    urls.path("about/", urls.include("about.urls")),
    urls.path("blog/", urls.include("deblog.urls")),
    urls.path("r/", urls.include("redirects.urls")),
    urls.path("", urls.include("core.urls")),
    urls.path("", urls.include("memes.urls")),
    urls.path("", urls.include("txt_pages.urls")),
    urls.path("oficina/", admin.site.urls),
]

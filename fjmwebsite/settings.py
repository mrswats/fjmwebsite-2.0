import os
from pathlib import Path
from typing import Any

import sentry_sdk
from configurations import Configuration
from sentry_sdk.integrations.django import DjangoIntegration

from sentry import sampler


class BaseSettings(Configuration):
    BASE_DIR = Path(__file__).resolve().parent.parent

    AUTH_USER_MODEL = "core.FjmwebsiteUser"

    LOGIN_REDIRECT_URL = "/oficina/"

    INSTALLED_APPS = [
        "django.contrib.admin",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.messages",
        "django.contrib.staticfiles",
        "core.apps.CoreConfig",
        "about.apps.AboutConfig",
        "memes.apps.MemesConfig",
        "deblog.apps.DeblogConfig",
        "redirects.apps.RedirectsConfig",
        "txt_pages.apps.TXTPagesConfig",
        "jovelldotdev",
        "heroicons",
        "tailwind",
    ]

    MIDDLEWARE = [
        "middlewares.secure_middleware.set_secure_headers",
        "django.middleware.security.SecurityMiddleware",
        "whitenoise.middleware.WhiteNoiseMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.middleware.clickjacking.XFrameOptionsMiddleware",
    ]

    PASSWORD_HASHERS = [
        "django.contrib.auth.hashers.Argon2PasswordHasher",
    ]

    DATABASE_NAME = os.getenv("DATABASE_NAME", "fjmwebsite.db")

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": DATABASE_NAME,
        },
    }

    CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
            "LOCATION": "unique-snowflake",
        }
    }

    SESSION_ENGINE = "django.contrib.sessions.backends.cache"

    DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

    ROOT_URLCONF = "fjmwebsite.urls"

    TEMPLATES = [
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": [],
            "APP_DIRS": True,
            "OPTIONS": {
                "builtins": [
                    "tailwind.templatetags.tailwind_tags",
                    "heroicons.templatetags.heroicons",
                    "deblog.templatetags.blog_filters",
                ],
                "context_processors": [
                    "django.template.context_processors.debug",
                    "django.template.context_processors.request",
                    "django.contrib.auth.context_processors.auth",
                    "django.contrib.messages.context_processors.messages",
                ],
            },
        },
    ]

    WSGI_APPLICATION = "fjmwebsite.wsgi.application"
    ASGI_APPLICATION = "fjmwebsite.asgi.application"

    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True

    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "default": {
                "format": "[{asctime}][{name}][{levelname}][{module}] {message}",
                "style": "{",
            },
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "default",
            },
        },
        "loggers": {
            "fjmwebsite": {
                "handlers": ["console"],
                "level": os.getenv("DJANGO_WEB_LOG_LEVEL", "INFO"),
                "propagate": True,
            },
            "django": {
                "handlers": ["console"],
                "level": os.getenv("DJANGO_LOG_LEVEL", "INFO"),
                "propagate": True,
            },
        },
    }

    BLOG_PAGE_SIZE = os.getenv("BLOG_PAGE_SIZE", 10)

    LANGUAGE_CODE = "en-us"
    TIME_ZONE = "Europe/Andorra"
    USE_I18N = True
    USE_L10N = True
    USE_TZ = True

    STATIC_URL = "/static/"
    STATIC_ROOT = BASE_DIR / "fjmwebsite/static"

    WHITENOISE_ALLOW_ALL_ORIGINS = False
    WHITENOISE_MANIFEST_STRICT = False

    TAILWIND_APP_NAME = "jovelldotdev"
    TAILWIND_CSS_PATH = "css/dist/styles.css"

    MARKDOWN_EXTENSIONS = [
        "fenced_code",
        "codehilite",
    ]

    MARKDOWN_EXTENSIONS_CONFIG = {
        "codehilite": {
            "use_pygments": True,
        }
    }


class Production(BaseSettings):
    DEBUG = False
    ALLOWED_HOSTS = os.getenv("DJANGO_ALLOWED_HOSTS", "").split(",")

    SECRET_KEY = os.getenv("DJANGO_SECRET_KEY")

    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True
    SECURE_HSTS_SECONDS = 30

    SECURE_SSL_REDIRECT = True

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": os.getenv("FJMWEBSITE_DB_NAME", ""),
            "USER": os.getenv("FJMWEBSITE_DB_USER", ""),
            "PASSWORD": os.getenv("FJMWEBSITE_DB_PASS", ""),
            "HOST": os.getenv("FJMWEBSITE_DB_HOST", ""),
            "PORT": "5432",
        }
    }

    CACHES: dict[str, Any] = {
        "default": {
            "BACKEND": "django.core.cache.backends.redis.RedisCache",
            "LOCATION": os.getenv("FJMWEBSITE_REDIS_HOST", ""),
        }
    }

    @classmethod
    def pre_setup(cls) -> None:
        DJANGO_SENTRY_DSN = os.getenv("DJANGO_SENTRY_DSN")

        if DJANGO_SENTRY_DSN:
            sentry_sdk.init(
                dsn=DJANGO_SENTRY_DSN,
                integrations=[DjangoIntegration()],
                traces_sample_rate=sampler.traces_sampler,
                send_default_pii=True,
                environment="Production",
                profiles_sample_rate=0.05,
            )

        super().pre_setup()


class Local(BaseSettings):
    DEBUG = True
    ALLOWED_HOSTS: list[str] = []

    SECRET_KEY = "django-development-key"

    INTERNAL_IPS = ["127.0.0.1", "localhost"]

    ROOT_URLCONF = "fjmwebsite.local_urls"

    SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"

    @property
    def INSTALLED_APPS(self):
        return [
            "whitenoise.runserver_nostatic",
            *super().INSTALLED_APPS,
            "django_browser_reload",
        ]

    @property
    def MIDDLEWARE(self):
        return [
            *super().MIDDLEWARE,
            "django_browser_reload.middleware.BrowserReloadMiddleware",
        ]

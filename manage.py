#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import contextlib
import os
import sys


def main() -> int:
    """Run administrative tasks."""
    with contextlib.suppress(ImportError):
        import dotenv

        dotenv.load_dotenv(dotenv.find_dotenv())

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fjmwebsite.settings")
    os.environ.setdefault("DJANGO_CONFIGURATION", "Local")

    try:
        from configurations.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
    return 0


if __name__ == "__main__":
    raise SystemExit(main())

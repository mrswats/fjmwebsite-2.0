from datetime import datetime

from django import shortcuts
from django.http import HttpRequest
from django.http import HttpResponse


def divendres(request: HttpRequest) -> HttpResponse:
    dia_de_la_setmana = datetime.now().isoweekday()
    return shortcuts.render(
        request, "memes/divendres.html", {"dia_de_la_setmana": dia_de_la_setmana}
    )

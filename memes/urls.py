from django.urls import path
from django.views.generic import TemplateView

from memes import views


urlpatterns = [
    path("blank/", TemplateView.as_view(template_name="memes/blank.html"), name="blank"),
    path("418/", TemplateView.as_view(template_name="memes/teapot.html"), name="teapot"),
    path("valles/", TemplateView.as_view(template_name="memes/valles.html"), name="valles"),
    path("divendres/", views.divendres, name="divendres"),
]

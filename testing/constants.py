from datetime import datetime as dt

test_title = "Test Title Blog Post!?"
test_slug = "test-title-blog-post"
test_year = 2020
test_month = 12
test_day = 31
test_date = dt(test_year, test_month, test_day)

SECURITY_MIDDLEWARE = "middlewares.secure_middleware.set_secure_headers"

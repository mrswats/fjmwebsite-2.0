import os

from fjmwebsite.settings import Local


class Test(Local):
    INSTALLED_APPS = [
        "django.contrib.admin",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.staticfiles",
        "core.apps.CoreConfig",
        "memes.apps.MemesConfig",
        "about.apps.AboutConfig",
        "deblog.apps.DeblogConfig",
        "redirects.apps.RedirectsConfig",
        "txt_pages.apps.TXTPagesConfig",
        "jovelldotdev",
        "heroicons",
        "tailwind",
    ]

    MIDDLEWARE = [
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
    ]

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": ":memory:",
        }
    }

    SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

    CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }


class CITest(Test):
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": os.getenv("POSTGRES_DB", ""),
            "USER": os.getenv("POSTGRES_USER", ""),
            "PASSWORD": os.getenv("POSTGRES_PASSWORD", ""),
            "HOST": os.getenv("POSTGRES_HOST", "postgres"),
            "PORT": "5432",
        }
    }

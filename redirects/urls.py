from django import urls

from redirects import views

urlpatterns = [
    urls.path("mail/", views.redirect_mailto, name="redirect-mail"),
    urls.path("<slug:page_name>/", views.redirects_main_view, name="redirect"),
]

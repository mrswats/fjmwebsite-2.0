from django.db import models


class Redirect(models.Model):
    url = models.CharField(max_length=50, verbose_name="URL Name")
    target = models.CharField(max_length=300, verbose_name="Target URL")
    active = models.BooleanField(default=True)

    def __str__(self) -> str:
        return f"{self.url} -> {self.target[:15]}..."

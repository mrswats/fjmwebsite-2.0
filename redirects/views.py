from django import shortcuts
from django.contrib import auth
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.http import HttpResponseRedirect

from redirects import models


def redirects_main_view(
    request: HttpRequest,
    page_name: str,
) -> HttpResponseRedirect | HttpResponseNotFound:
    page = shortcuts.get_object_or_404(models.Redirect, url=page_name, active=True)
    return HttpResponseRedirect(redirect_to=page.target)


def redirect_mailto(request: HttpRequest) -> HttpResponse:
    User = auth.get_user_model()
    me = User.objects.first()
    return HttpResponse(content=f'<meta http-equiv="refresh" content="0;url=mailto:{me.email}" />')

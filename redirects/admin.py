from django import urls
from django.contrib import admin

from redirects import models


@admin.register(models.Redirect)
class RedirectAdmin(admin.ModelAdmin):
    list_display = (
        "active",
        "url",
        "target",
        "full_url",
    )

    list_display_links = ("url",)

    def full_url(self, obj: models.Redirect) -> str:
        obj_url = urls.reverse("redirect", kwargs={"page_name": obj.url})
        return f"https://www.jovell.dev{obj_url}"

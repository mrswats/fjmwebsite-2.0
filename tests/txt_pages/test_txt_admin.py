import pytest

from txt_pages.admin import TXTPageAdmin
from txt_pages.models import TXTPage


@pytest.fixture
def txt_admin(admin_site):
    return TXTPageAdmin(model=TXTPage, admin_site=admin_site)


@pytest.mark.parametrize(
    "field",
    [
        "name",
        "url",
        "body",
    ],
)
def test_txt_admin_list_display(field, txt_admin):
    assert field in txt_admin.list_display


@pytest.mark.parametrize(
    "field",
    [
        "name",
        "url",
        "body",
    ],
)
def test_txt_admin_fields(field, txt_admin):
    assert field in txt_admin.fields


def test_txt_admin_get_readonly_fields_create(txt_admin):
    fields = txt_admin.get_readonly_fields(request=None, obj=None)
    assert fields == ()


def test_txt_admin_get_readonly_fields_edit(txt_admin):
    fields = txt_admin.get_readonly_fields(request=None, obj=TXTPage(name="name", body="body"))
    assert fields == ("name", "url")

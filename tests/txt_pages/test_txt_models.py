import pytest
from django.db import IntegrityError

from txt_pages.models import TXTPage


@pytest.mark.django_db
def test_uniqueness():
    kwargs = {
        "name": "banner.txt",
        "url": "my_url.txt",
        "body": "<body></body>",
    }

    TXTPage.objects.create(**kwargs)

    with pytest.raises(IntegrityError):
        TXTPage.objects.create(**kwargs)


@pytest.mark.django_db
def test_txt_repr():
    txt = TXTPage.objects.create(name="banner.txt", body="body")
    assert str(txt) == txt.name


@pytest.mark.django_db
def test_uniqueness_is_together():
    TXTPage.objects.create(name="banner.txt", body="body")
    TXTPage.objects.create(name="banner.txt", url="my_coool_url.txt", body="body")

    assert TXTPage.objects.all().count() == 2

from http import HTTPStatus

import pytest
from django.http import Http404
from django.urls.exceptions import NoReverseMatch

from txt_pages.models import TXTPage
from txt_pages.views import render_txt


@pytest.fixture
def txt():
    return TXTPage.objects.create(
        name="banner.txt",
        url="my_url_banner.txt",
        body="<body></body>",
    )


@pytest.fixture
def txt_request(request_):
    return request_.get("/banner.txt")


def test_txt_pages_urls(url):
    assert url("txt", page_name="banner.txt") == "/banner.txt"


def test_txt_pages_url_ends_with_txt(url):
    with pytest.raises(NoReverseMatch):
        url("txt", page_name="banner")


@pytest.mark.django_db
def test_txt_view_not_found(txt_request):
    with pytest.raises(Http404):
        render_txt(txt_request, page_name="banner.txt")


@pytest.mark.django_db
def test_txt_view_content_ends_with_newline_character(txt_request, txt):
    response = render_txt(txt_request, page_name=txt.url)
    assert response.content.decode()[-1] == "\n"


@pytest.mark.django_db
def test_txt_view_with_page_by_url(txt_request, txt):
    response = render_txt(txt_request, page_name=txt.url)
    assert response.status_code == HTTPStatus.OK
    assert response.headers["Content-Type"] == "text/plain"
    assert txt.body in response.content.decode()

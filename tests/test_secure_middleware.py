import pytest
from django.http import HttpResponse
from django.test import RequestFactory

from middlewares.secure_middleware import set_secure_headers


@pytest.fixture
def make_response(test_url):
    def _(url: str = test_url):
        middleware = set_secure_headers(get_response=lambda request: HttpResponse("hi :)"))
        request = RequestFactory().get(url)
        return middleware(request)

    return _


@pytest.mark.parametrize(
    "header_name, header_value",
    [
        ("Referrer-Policy", "strict-origin-when-cross-origin"),
        ("Strict-Transport-Security", "max-age=31536000"),
        ("Cache-Control", "no-store"),
        (
            "Content-Security-Policy",
            ("default-src 'self'; script-src 'self'; style-src 'self'; object-src 'none'"),
        ),
        ("Permissions-Policy", "geolocation=(), microphone=(), camera=()"),
    ],
)
@pytest.mark.urls("testing.urls")
def test_secure_headers_in_response(make_response, header_name, header_value):
    response = make_response()
    assert response.headers.get(header_name) == header_value


@pytest.mark.parametrize(
    "header_name, header_value",
    [
        ("Content-Type", "text/html; charset=utf-8"),
    ],
)
@pytest.mark.urls("testing.urls")
def test_secure_headers_favicon_contains_default_headers(make_response, header_name, header_value):
    response = make_response("/favicon.ico")
    assert response.headers.get(header_name) == header_value

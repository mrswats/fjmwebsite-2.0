import pytest

from sentry.sampler import traces_sampler


def get_ctx(path: str) -> dict[str, dict[str, str]]:
    return {
        "wsgi_environ": {"PATH_INFO": path},
    }


@pytest.mark.parametrize(
    "path",
    [
        "/status/",
        "/static/",
    ],
)
def test_sampler_status_request(path):
    assert traces_sampler(get_ctx(path)) == 0


def test_sampler_any_path():
    assert traces_sampler(get_ctx("/any-other-path/")) == 1

from unittest.mock import call
from unittest.mock import patch

import pytest

from build import main


@pytest.mark.parametrize(
    "command, cargs, ckwargs",
    [
        ("tailwind", ["install"], {}),
        ("tailwind", ["build"], {}),
        ("collectstatic", [], {"verbosity": 1, "interactive": False}),
        ("migrate", [], {"verbosity": 1}),
    ],
)
@pytest.mark.django_db
def test_build(command, cargs, ckwargs):
    with patch("django.core.management.call_command") as cmd:
        main()

    assert call(command, *cargs, **ckwargs) in cmd.call_args_list


def test_build_script_returns_ok_status_code():
    with patch("django.core.management.call_command"):
        ret = main()

    assert ret == 0

import pytest
import time_machine
from django import urls
from django.contrib import admin
from django.test import RequestFactory
from django.utils import timezone

from about import models as about_models
from core import models as core_models
from deblog import models as blog_models
from testing.constants import test_date
from testing.constants import test_title


@pytest.fixture
def set_time():
    with time_machine.travel(test_date, tick=False):
        yield test_date


@pytest.fixture
def url():
    def _(url_name: str, **kwargs) -> str:
        return urls.reverse(url_name, kwargs=kwargs)

    return _


@pytest.fixture
def user():
    return core_models.FjmwebsiteUser.objects.create_superuser(
        username="fjm",
        email="test@mail.com",
        twitter_handle="@mrswats",
    )


@pytest.fixture
def admin_site():
    return admin.AdminSite()


@pytest.fixture
def login(client, user):
    client.force_login(user)


@pytest.fixture
def blog_post(user):
    return blog_models.Post.objects.create(
        author=user,
        title=test_title,
        text="# very cool blog post",
        _tags="tag1, tag2, tag3",
        published_date=test_date,
    )


@pytest.fixture
def unpublished_post(user):
    return blog_models.Post.objects.create(
        author=user,
        title=test_title,
        text="# very cool blog post",
        _tags="tag1, tag2, tag3",
        published_date=None,
    )


@pytest.fixture
def project():
    return about_models.Project.objects.create(
        title=test_title,
        description="Very cool description about my project",
        href="http://example.com",
        link="link-to-the-stuff",
    )


@pytest.fixture
def talk():
    return about_models.Talk.objects.create(
        title="best talk ever",
        date=timezone.now(),
    )


@pytest.fixture
def bio():
    return about_models.Biography.objects.create(body="This is my biography")


@pytest.fixture
def now():
    return about_models.Now.objects.create(body="this is what I am doing now")


bio_v2 = bio


@pytest.fixture
def test_url(url):
    return url("test-url")


@pytest.fixture
def request_():
    return RequestFactory()

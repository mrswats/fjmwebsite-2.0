from http import HTTPStatus

import pytest

from redirects import models
from redirects.admin import RedirectAdmin


@pytest.fixture
def admin_instance(admin_site):
    return RedirectAdmin(model=models.Redirect, admin_site=admin_site)


@pytest.fixture
def redirect_obj():
    return models.Redirect.objects.create(url="redirect-url", target="target-url")


@pytest.fixture
def mail_url(url):
    return url("redirect-mail")


def test_redirect_url(url):
    assert url("redirect", page_name="this-is-a-page") == "/r/this-is-a-page/"


def test_redirect_mailto_url(mail_url):
    assert mail_url == "/r/mail/"


@pytest.mark.django_db
def test_redirect_not_found(client, url):
    redirect_url = url("redirect", page_name="this-does-not-exist")
    response = client.get(redirect_url)
    assert response.status_code == HTTPStatus.NOT_FOUND


@pytest.mark.django_db
@pytest.mark.usefixtures("redirect_obj")
def test_redirect_returns_redirect_status_code(client, url):
    redirect_url = url("redirect", page_name="redirect-url")
    response = client.get(redirect_url, follow=False)
    assert response.status_code == HTTPStatus.FOUND


@pytest.mark.django_db
@pytest.mark.usefixtures("redirect_obj")
def test_redirect_returns_location_header(client, url):
    redirect_url = url("redirect", page_name="redirect-url")
    response = client.get(redirect_url, follow=False)
    assert response.status_code == HTTPStatus.FOUND
    assert response.headers.get("Location") == "target-url"


@pytest.mark.django_db
@pytest.mark.usefixtures("user")
def test_redirect_mailto(client, url):
    mail_url = url("redirect-mail")
    response = client.get(mail_url)
    assert response.content == (
        b'<meta http-equiv="refresh" content="0;url=mailto:test@mail.com" />'
    )


@pytest.mark.parametrize(
    "field",
    [
        "url",
        "target",
        "active",
    ],
)
@pytest.mark.django_db
def test_redirect_model_attrs(redirect_obj, field):
    assert hasattr(redirect_obj, field)


@pytest.mark.django_db
def test_redirect_model_str(redirect_obj):
    assert str(redirect_obj) == "redirect-url -> target-url..."


@pytest.mark.django_db
def test_redirect_admin_full_url(redirect_obj, admin_instance):
    assert admin_instance.full_url(redirect_obj) == "https://www.jovell.dev/r/redirect-url/"

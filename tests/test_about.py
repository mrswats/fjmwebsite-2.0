from http import HTTPStatus

import pytest

from about.models import Biography


@pytest.mark.parametrize(
    "url_name, resolved_url",
    [
        ("about-me", "/about/me/"),
        ("projects", "/about/projects/"),
        ("bots", "/about/bots/"),
        ("talks", "/about/talks/"),
        ("now", "/about/now/"),
    ],
)
def test_about_urls(url, url_name, resolved_url):
    assert url(url_name) == resolved_url


@pytest.mark.parametrize("url_name", ["bots"])
def test_template_view_returns_ok_status_code(client, url, url_name):
    resolved_url = url(url_name)
    response = client.get(resolved_url)
    assert response.status_code == HTTPStatus.OK


@pytest.mark.parametrize(
    "url_name",
    ["projects", "talks", "about-me"],
)
@pytest.mark.django_db
def test_view_returns_ok_status_code(client, url, url_name):
    response = client.get(url(url_name))
    assert response.status_code == HTTPStatus.OK


@pytest.mark.parametrize("field", ("date", "body"))
@pytest.mark.django_db
def test_biography_fields(bio, field):
    assert hasattr(bio, field)


@pytest.mark.usefixtures("set_time")
@pytest.mark.django_db
def test_biography_repr(bio):
    assert str(bio) == f"<Bio (v{bio.id}) - 2020-12-31>"


@pytest.mark.django_db
def test_biography_ordering(bio, bio_v2):
    assert Biography.objects.first() == bio_v2


@pytest.mark.parametrize(
    "url_name, template_name",
    [
        ("bots", "about/bots.html"),
    ],
)
def test_template_view(client, url, url_name, template_name):
    resolved_url = url(url_name)
    response = client.get(resolved_url)
    assert any(template_name == template.name for template in response.templates)


@pytest.mark.parametrize(
    "url_name, template_name",
    [
        ("projects", "about/projects.html"),
        ("talks", "about/talks.html"),
        ("about-me", "about/me.html"),
        ("now", "about/now.html"),
    ],
)
@pytest.mark.django_db
def test_view_renders_correct_template(client, url, url_name, template_name):
    response = client.get(url(url_name))
    assert template_name in (template.name for template in response.templates)


def test_url_not_found_returns_not_found_status_code(client):
    response = client.get("non-existent-url/", follow=True)
    assert response.status_code == HTTPStatus.NOT_FOUND


def test_url_not_found_uses_404_template(client):
    response = client.get("non-existent-url/", follow=True)
    assert response.templates[0].name == "404.html"


@pytest.mark.django_db
def test_projects_has_context(client, url, project):
    response = client.get(url("projects"))
    assert project in response.context["projects"]


@pytest.mark.django_db
def test_talks_has_context(client, url, talk):
    response = client.get(url("talks"))
    assert talk in response.context["talks"]


@pytest.mark.django_db
def test_about_has_context(client, url, bio):
    response = client.get(url("about-me"))
    assert response.context["biography"] == bio


@pytest.mark.parametrize(
    "field",
    [
        "title",
        "description",
        "href",
        "link",
    ],
)
@pytest.mark.django_db
def test_about_models_project(project, field):
    assert hasattr(project, field)


@pytest.mark.django_db
def test_about_models_project_str(project):
    assert str(project) == project.title


@pytest.mark.parametrize(
    "field",
    [
        "abstract",
        "date",
        "event",
        "location",
        "slides_link",
        "title",
        "video_link",
    ],
)
@pytest.mark.django_db
def test_about_models_talk(talk, field):
    assert hasattr(talk, field)


@pytest.mark.django_db
def test_about_models_talk_str(talk):
    assert str(talk) == f"{talk.title} ({talk.date.strftime('%Y-%m-%d')})"


@pytest.mark.django_db
def test_about_now_model_str(now):
    assert str(now) == now.created.isoformat()


@pytest.mark.django_db
def test_about_now_get_absolute_url(now):
    assert now.get_absolute_url() == "/about/now/"


@pytest.mark.django_db
def test_about_now_html(now):
    assert now.html == "<p>this is what I am doing now</p>"


@pytest.mark.django_db
def test_about_now_view_context_no_data(client, url):
    response = client.get(url("now"))
    assert response.context["now"] is None


@pytest.mark.django_db
def test_about_now_view_context(client, url, now):
    response = client.get(url("now"))
    assert now == response.context["now"]

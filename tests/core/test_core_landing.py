import re
from datetime import datetime
from http import HTTPStatus

import pytest


@pytest.fixture
def landing_page(client, url):
    return client.get(url("home"))


def test_landing_page_has_context(landing_page):
    assert landing_page.context["socials"] == ["github", "gitlab", "bluesky"]
    assert landing_page.status_code == HTTPStatus.OK
    assert "home.html" in [template.name for template in landing_page.templates]


def test_landing_copyright_in_footer(landing_page):
    current_year = datetime.now().year
    response_html = landing_page.content.decode()

    assert re.search(rf'Copyright {current_year} by <a href="/">Ferran Jovell</a>', response_html)

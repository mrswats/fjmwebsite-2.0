import pytest

from testing.constants import test_day
from testing.constants import test_month
from testing.constants import test_slug
from testing.constants import test_year


@pytest.mark.parametrize(
    "url_name, resolved_url",
    [
        ("home", "/"),
        ("status", "/status/"),
        ("favicon", "/favicon.ico"),
        ("til", "/til/"),
    ],
)
def test_core_urls(url, url_name, resolved_url):
    assert url(url_name) == resolved_url


def test_til_detail_url(url):
    assert (
        url("til-detail", year=test_year, month=test_month, day=test_day, slug=test_slug)
        == f"/til/{test_year}/{test_month}/{test_day}/{test_slug}/"
    )


def test_til_view_redirects_to_blog(url, client):
    response = client.get(url("til"))
    assert response.headers["Location"] == "/blog/?t=TIL"


def test_til_detail_redirects_to_blog(url, client):
    response = client.get(
        url("til-detail", year=test_year, month=test_month, day=test_day, slug=test_slug)
    )
    assert response.headers["Location"] == f"/blog/{test_year}/{test_month}/{test_slug}/"

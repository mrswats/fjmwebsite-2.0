from http import HTTPStatus

import pytest

from core.views import favicon


@pytest.fixture
def favicon_url(url):
    return url("favicon")


def test_favicon_returns_ok_status_codew(request_, favicon_url):
    response = favicon(request_.get(favicon_url))
    assert response.status_code == HTTPStatus.OK
    assert response["Cache-Control"] == "max-age=86400, immutable, public"
    assert response["Content-Type"] == "image/png"
    assert len(response.getvalue()) > 0


@pytest.mark.parametrize(
    "header_name, header_value",
    [
        ("Content-Type", "image/png"),
        ("Content-Length", "2536"),
        ("Content-Disposition", 'inline; filename="logo_fjm.png"'),
        ("Cache-Control", "max-age=86400, immutable, public"),
    ],
)
def test_favicon_cache_headers(request_, favicon_url, header_name, header_value):
    response = favicon(request_.get(favicon_url))
    assert response.headers[header_name] == header_value

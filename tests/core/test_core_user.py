import pytest

from core.models import FjmwebsiteUser


@pytest.fixture
def user_m():
    return FjmwebsiteUser(
        username="fjm",
        email="test@mail.com",
    )


@pytest.mark.parametrize(
    "field",
    [
        "username",
        "email",
        "password",
        "first_name",
        "last_name",
        "twitter_handle",
    ],
)
def test_model_user_fields(user_m, field):
    assert hasattr(user_m, field)

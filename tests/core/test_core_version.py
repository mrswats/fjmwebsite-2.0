import tomllib
from http import HTTPStatus


def get_pyrpoject_version() -> str:
    with open("pyproject.toml", "rb") as f:
        pyproject_data = tomllib.load(f)

    return pyproject_data["project"]["version"]


def test_version_view(client, url):
    response = client.get(url("version"))
    assert response.status_code == HTTPStatus.OK
    assert response.json() == {"version": get_pyrpoject_version()}

from http import HTTPStatus

from core.views import status


def test_status_view_returns_ok_status_code(url, request_):
    healthcheck_url = url("status")
    response = status(request_.get(healthcheck_url))
    assert response.status_code == HTTPStatus.OK


def test_status_view_returns_appropriate_response(url, client):
    healthcheck_url = url("status")
    response = client.get(healthcheck_url)
    assert response.json() == {"status": "healthy!"}


def test_status_sends_no_cache_header(url, request_):
    healthcheck_url = url("status")
    response = status(request_.get(healthcheck_url))
    assert (
        response.headers["Cache-Control"]
        == "max-age=0, no-cache, no-store, must-revalidate, private"
    )

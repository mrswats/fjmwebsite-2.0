import pytest

from about import models
from about.admin import BiographyAdmin
from about.admin import ProjectAdmin


@pytest.fixture
def admin_instance(admin_site):
    return ProjectAdmin(model=models.Project, admin_site=admin_site)


@pytest.fixture
def bio_admin(admin_site):
    return BiographyAdmin(model=models.Biography, admin_site=admin_site)


@pytest.mark.django_db
def test_admin_short_description(admin_instance, project):
    short_description = admin_instance.short_description(project)
    assert len(short_description) <= 50


@pytest.mark.django_db
def test_admin_bio_version_column(bio_admin, bio):
    assert bio_admin.version(bio) == f"v{bio.id}"


@pytest.mark.django_db
def test_admin_bio_body(bio_admin, bio):
    bio.body = "A" * 50
    assert len(bio_admin.short_body(bio)) <= 25

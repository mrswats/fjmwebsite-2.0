import pytest

from deblog import models
from deblog.admin import PostAdmin
from deblog.admin import publish_post
from deblog.admin import unpublish_post


@pytest.fixture
def admin_instance(admin_site):
    return PostAdmin(model=models.Post, admin_site=admin_site)


def test_admin_date_hirearchy(admin_instance):
    assert admin_instance.date_hierarchy == "created_date"


@pytest.mark.parametrize(
    "field",
    [
        "created_date",
        "published_date",
        "author",
        "title",
        "_tags",
        "text",
    ],
)
def test_admin_fields(admin_instance, field):
    assert field in admin_instance.fields


@pytest.mark.parametrize(
    "field",
    [
        "created_date",
        "is_published",
        "published_date",
        "title",
        "tags",
        "view_post",
    ],
)
def test_admin_list_display(admin_instance, field):
    assert field in admin_instance.list_display


def test_admin_autocomplete_fields(admin_instance):
    assert admin_instance.autocomplete_fields == ("author",)


@pytest.mark.django_db
def test_admin_view_post(blog_post, admin_instance):
    absolute_url = blog_post.get_absolute_url()
    assert admin_instance.view_post(blog_post) == f'<a href="{absolute_url}">{absolute_url}</a>'


@pytest.mark.django_db
@pytest.mark.usefixtures("blog_post")
def test_admin_publish_post_action(admin_instance):
    queryset = models.Post.objects.all()
    publish_post(modeladmin=admin_instance, request=None, queryset=queryset)
    assert all(bool(post.published_date) for post in queryset)


@pytest.mark.django_db
@pytest.mark.usefixtures("blog_post")
def test_admin_unpublish_post_action(admin_instance):
    queryset = models.Post.objects.all()
    unpublish_post(modeladmin=admin_instance, request=None, queryset=queryset)
    assert all(not bool(post.published_date) for post in queryset)

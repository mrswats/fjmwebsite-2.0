import re
from http import HTTPStatus

import pytest

from testing.constants import test_month
from testing.constants import test_slug
from testing.constants import test_year


@pytest.mark.parametrize(
    "url_name, url_kwargs",
    [
        ("blog", {}),
        ("post", {"year": test_year, "month": test_month, "slug": test_slug}),
        ("post-year", {"year": test_year}),
        ("preview", {"slug": test_slug}),
    ],
)
@pytest.mark.usefixtures("blog_post", "unpublished_post", "login")
@pytest.mark.django_db
def test_blog_view_returns_ok_status_code(client, url, url_name, url_kwargs):
    resolved_url = url(url_name, **url_kwargs)
    response = client.get(resolved_url)
    assert response.status_code == HTTPStatus.OK


@pytest.mark.usefixtures("blog_post", "unpublished_post", "login")
@pytest.mark.parametrize(
    "url_name, template_name, url_kwargs",
    [
        ("blog", "blog/list.html", {}),
        ("post", "blog/post.html", {"year": test_year, "month": test_month, "slug": test_slug}),
        ("post-year", "blog/list.html", {"year": test_year}),
        ("preview", "blog/post_preview.html", {"slug": test_slug}),
    ],
)
@pytest.mark.django_db
def test_blog_templates(client, url, url_name, template_name, url_kwargs):
    resolved_url = url(url_name, **url_kwargs)
    response = client.get(resolved_url)
    assert any(template_name == template.name for template in response.templates)


@pytest.mark.usefixtures("unpublished_post", "blog_post", "login")
@pytest.mark.parametrize(
    "page, kwargs, regex",
    [
        ("blog", {}, "Deblog"),
        ("preview", {"slug": test_slug}, "Preview"),
    ],
)
@pytest.mark.django_db
def test_blog_page_matches_regex(client, url, page, kwargs, regex):
    resolved_url = url(page, **kwargs)
    response = client.get(resolved_url)
    assert re.search(regex, response.content.decode())

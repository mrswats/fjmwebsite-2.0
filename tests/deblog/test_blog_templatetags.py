import pytest

from deblog.templatetags.blog_filters import querystring


@pytest.mark.parametrize(
    "kwargs, expected",
    [
        ({}, ""),
        ({"foo": ""}, ""),
        ({"foo": None}, ""),
        ({"foo": "bar"}, "?foo=bar"),
        ({"foo": " bar "}, "?foo=bar"),
        ({"foo": "bar", "baz": None}, "?foo=bar"),
        ({"foo": "bar", "baz": "wat"}, "?foo=bar&baz=wat"),
    ],
)
def test_querystring(kwargs, expected):
    assert querystring(**kwargs) == expected

from http import HTTPStatus

import pytest

from testing.constants import test_slug


def test_blog_preview_url(preview_url):
    assert preview_url == f"/blog/preview/{test_slug}/"


@pytest.mark.django_db
def test_model_absolute_url_unpublished_post(unpublished_post):
    assert unpublished_post.get_absolute_url() == f"/blog/preview/{test_slug}/"


@pytest.mark.django_db
def test_preview_page_returns_not_found_for_unauthorized_user(
    client, preview_url, unpublished_post
):
    response = client.get(preview_url, follow=True)
    assert response.status_code == HTTPStatus.NOT_FOUND


@pytest.mark.django_db
@pytest.mark.usefixtures("login")
def test_preview_page_allows_authorized_user(client, login, preview_url, unpublished_post):
    response = client.get(preview_url)
    assert response.status_code == HTTPStatus.OK


@pytest.mark.django_db
@pytest.mark.usefixtures("login")
def test_preview_page(login, client, url, unpublished_post):
    preview_url = url("preview", slug=unpublished_post.slug)
    response = client.get(preview_url)
    assert response.context["post"] == unpublished_post

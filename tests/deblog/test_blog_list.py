import datetime as dt

import pytest
from django.core.paginator import Page

from deblog.models import Post
from testing.constants import test_year


@pytest.fixture
def create_posts(user):
    Post.objects.bulk_create(
        [
            Post(
                author=user,
                title=f"title{index}",
                slug=f"title{index}",
                published_date=dt.datetime(2020 + index, 1, 1),
            )
            for index in range(3)
        ]
    )


def test_blog_list_url(blog_url):
    assert blog_url == "/blog/"


def test_blog_list_per_year_url(blog_year_url):
    assert blog_year_url == f"/blog/{test_year}/"


@pytest.mark.django_db
def test_blog_list_filters_by_existing_tag(client, blog_url, blog_post):
    tag = "tag1"
    response = client.get(blog_url, {"t": tag})
    assert blog_post in response.context["posts"].object_list


@pytest.mark.django_db
def test_blog_list_filters_by_non_existing_tag(client, blog_url, blog_post):
    tag = "tag0"
    response = client.get(blog_url, {"t": tag})
    assert blog_post not in response.context["posts"].object_list


@pytest.mark.django_db
def test_blog_list_does_not_contain_published_posts(client, blog_url, blog_post):
    response = client.get(blog_url)
    assert blog_post in response.context["posts"]


@pytest.mark.django_db
def test_blog_list_contains_selected_year_post(client, blog_year_url, blog_post):
    response = client.get(blog_year_url)
    years = {post.published_date.year for post in response.context["posts"]}
    assert years == {test_year}


@pytest.mark.django_db
def test_blog_list_does_not_contain_other_years_posts(client, url, blog_post):
    blog_year_url = url("post-year", year=2000)
    response = client.get(blog_year_url)
    years = {post.published_date.year for post in response.context["posts"]}
    assert years == set()


@pytest.mark.django_db
def test_blog_list_does_not_contain_unpublished_posts(client, blog_url, unpublished_post):
    response = client.get(blog_url)
    assert not response.context["posts"].object_list.exists()


@pytest.mark.django_db
def test_blog_response_is_paginated(client, blog_url, blog_post):
    response = client.get(blog_url)
    assert isinstance(response.context["posts"], Page)


@pytest.mark.django_db
def test_blog_year_query_no_years(user, client, blog_url):
    assert not client.get(blog_url).context["years"].exists()


@pytest.mark.django_db
@pytest.mark.usefixtures("create_posts")
def test_blog_year_query(user, client, blog_url):
    response = client.get(blog_url)
    assert list(response.context["years"]) == [2020, 2021, 2022]

import pytest

from testing.constants import test_month
from testing.constants import test_slug
from testing.constants import test_year


def test_blog_detail_url(blog_detail_url):
    assert blog_detail_url == f"/blog/{test_year}/{test_month}/{test_slug}/"


@pytest.mark.django_db
def test_post_context(client, blog_detail_url, blog_post):
    response = client.get(blog_detail_url)
    assert response.context["post"] == blog_post


@pytest.mark.django_db
def test_post_detail_only_makes_one_call(django_assert_num_queries, client, blog_detail_url):
    with django_assert_num_queries(1):
        client.get(blog_detail_url)

import pytest

from testing.constants import test_date
from testing.constants import test_day
from testing.constants import test_month
from testing.constants import test_slug
from testing.constants import test_title
from testing.constants import test_year


@pytest.mark.django_db
def test_model_post_slug(blog_post):
    assert blog_post.slug == test_slug


@pytest.mark.django_db
def test_model_post_tags(blog_post):
    assert blog_post.tags == ["tag1", "tag2", "tag3"]


@pytest.mark.django_db
def test_model_post_markdown_html(blog_post):
    assert blog_post.html == "<h1>very cool blog post</h1>"


@pytest.mark.parametrize(
    "field",
    [
        "author",
        "title",
        "text",
        "html",
        "_tags",
        "slug",
        "created_date",
        "published_date",
    ],
)
@pytest.mark.django_db
def test_model_post_fields(blog_post, field):
    assert hasattr(blog_post, field)


@pytest.mark.django_db
def test_model_publish(set_time, blog_post):
    blog_post.published_date = None
    blog_post.save()
    blog_post.publish()
    assert blog_post.published_date.strftime("%Y-%m-%d") == test_date.strftime("%Y-%m-%d")


@pytest.mark.django_db
@pytest.mark.freeze_time(test_date)
def test_model_unpublish(set_time, blog_post):
    blog_post.unpublish()
    assert blog_post.published_date is None


@pytest.mark.django_db
def test_model_post_str(set_time, blog_post):
    assert str(blog_post) == f"({test_year}-{test_month}-{test_day}): {test_title}"


@pytest.mark.django_db
def test_model_is_published(blog_post):
    assert blog_post.is_published is True


@pytest.mark.django_db
def test_model_is_not_published(unpublished_post):
    assert unpublished_post.is_published is False


@pytest.mark.django_db
def test_model_absolute_url(blog_post):
    assert blog_post.get_absolute_url() == f"/blog/{test_year}/{test_month}/{test_slug}/"


@pytest.mark.django_db
def test_model_preview(blog_post):
    blog_post.text = "This is the first part.\n\nAnd this is the second."
    blog_post.save()
    assert blog_post.preview() == "<p>This is the first part.</p>"


@pytest.mark.django_db
def test_model_preview_text(blog_post):
    blog_post.text = "This is the first part.\n\nAnd this is the second."
    blog_post.save()
    assert blog_post.preview_text() == "This is the first part."


@pytest.mark.django_db
def test_model_preview_with_no_first_paragraph(blog_post):
    blog_post.text = "```python\n>>> print('hello world!')\n```"
    blog_post.save()
    assert blog_post.preview() == ""

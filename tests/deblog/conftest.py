import pytest

from testing.constants import test_month
from testing.constants import test_slug
from testing.constants import test_year


@pytest.fixture
def blog_url(url):
    return url("blog")


@pytest.fixture
def blog_detail_url(url):
    return url("post", year=test_year, month=test_month, slug=test_slug)


@pytest.fixture
def preview_url(url):
    return url("preview", slug=test_slug)


@pytest.fixture
def blog_year_url(url):
    return url("post-year", year=test_year)

from http import HTTPStatus

import pytest


@pytest.mark.parametrize(
    "url_name, resolved_url",
    [
        ("blank", "/blank/"),
        ("teapot", "/418/"),
        ("valles", "/valles/"),
        ("divendres", "/divendres/"),
    ],
)
def test_meme_urls(url, url_name, resolved_url):
    assert url(url_name) == resolved_url


@pytest.mark.parametrize(
    "url_name",
    [
        "blank",
        "teapot",
        "valles",
        "divendres",
    ],
)
def test_meme_template_view_returns_ok_status_code(client, url, url_name):
    resolved_url = url(url_name)
    response = client.get(resolved_url)
    assert response.status_code == HTTPStatus.OK


@pytest.mark.parametrize(
    "url_name, template_name",
    [
        ("blank", "memes/blank.html"),
        ("teapot", "memes/teapot.html"),
        ("valles", "memes/valles.html"),
        ("divendres", "memes/divendres.html"),
    ],
)
def test_memes_template_view(client, url, url_name, template_name):
    resolved_url = url(url_name)
    response = client.get(resolved_url)
    assert any(template_name == template.name for template in response.templates)

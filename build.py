import django
from configurations import importer
from django.core import management


def main() -> int:
    importer.install()
    django.setup()

    management.call_command("tailwind", "install")
    management.call_command("tailwind", "build")
    management.call_command("collectstatic", verbosity=1, interactive=False)
    management.call_command("migrate", verbosity=1)
    return 0


if __name__ == "__main__":
    raise SystemExit(main())

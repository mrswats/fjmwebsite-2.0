from typing import Any

IGNORED_PATHS = (
    "/status/",
    "/static/",
)


def traces_sampler(ctx: dict[str, Any]) -> float:
    path = ctx["wsgi_environ"].get("PATH_INFO", "")

    if any(path.startswith(ignored) for ignored in IGNORED_PATHS):
        return 0

    return 1

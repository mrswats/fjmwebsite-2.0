# FJM Website 2.0

[![pipeline status](https://gitlab.com/mrswats/fjmwebsite-2.0/badges/main/pipeline.svg)](https://gitlab.com/mrswats/fjmwebsite-2.0/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

This is my personal website, written in Django :)

## Installing dependencies:

```console
pip install -r test-requirements.txt
```

## Local Development

Install tailwind dependencies:

```console
./manage.py tailwind install
```

Run both the tailwind development server and the django development server in different shells:

```console
./manage.py tailwind start
./manage.py runserver
```

And the website should be available in `localhost:8000`.

## Running tests

Using pytest, using

```console
pytest
```

should suffice.

To run tests with a postgres database:

```console
pip install pytest-dotenv
cat <<EOF > .test.env
POSTGRES_DB="postgres"
POSTGRES_USER="postgres"
POSTGRES_PASSWORD="postgres"
POSTGRES_HOST="127.0.0.1"
DJANGO_CONFIGURATION=CITest
EOF
docker compose up -d
pytest
docker compose down --remove-orphans
```

### Run tests with coverage

```console
coverage run -m pytest
coverage report -m
```

## Linters and formatters

Managed by `pre-commit`. Install `pre-commit`

```console
pip install pre-commit
pre-commit run --all-files
```

## Semgrep

To run semgrep locally:

```console
pip install semgrep
semgrep --config=p/django
```

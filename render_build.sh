#!/usr/bin/env bash

set -euo pipefail

python -m pip install -r requirements.txt
python -m pip install .
python -m build

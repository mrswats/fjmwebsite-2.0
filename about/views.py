from django import shortcuts
from django.http import HttpRequest
from django.http import HttpResponse

from about import models


def about_me(request: HttpRequest) -> HttpResponse:
    bio = models.Biography.objects.first()
    return shortcuts.render(request, "about/me.html", {"biography": bio})


def projects(request: HttpRequest) -> HttpResponse:
    projects = models.Project.objects.all()
    return shortcuts.render(request, "about/projects.html", {"projects": projects})


def talks(request: HttpRequest) -> HttpResponse:
    talks = models.Talk.objects.order_by("-date")
    return shortcuts.render(request, "about/talks.html", {"talks": talks})


def now(request: HttpRequest) -> HttpResponse:
    now = models.Now.objects.first()
    return shortcuts.render(request, "about/now.html", {"now": now})

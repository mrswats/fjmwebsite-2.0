import re

import markdown
from django.conf import settings
from django.db import models
from django.urls import reverse


class Project(models.Model):
    title = models.CharField(max_length=25)
    description = models.TextField()
    href = models.URLField()
    link = models.CharField(max_length=25, default="", blank=True)

    def __str__(self) -> str:
        return self.title


class Talk(models.Model):
    title = models.CharField(max_length=250)
    date = models.DateField()
    event = models.CharField(max_length=250, default="")
    location = models.CharField(max_length=50)
    video_link = models.CharField(max_length=50, blank=True, default="")
    slides_link = models.CharField(max_length=50, blank=True, default="")
    abstract = models.TextField(blank=True, default="")

    def __str__(self) -> str:
        return f"{self.title} ({self.date.strftime('%Y-%m-%d')})"


class Biography(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    body = models.TextField()

    def __str__(self) -> str:
        return f"<Bio (v{self.id}) - {self.date.strftime('%Y-%m-%d')}>"

    class Meta:
        verbose_name_plural = "biographies"
        ordering = ["-id"]


class Now(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    body = models.TextField()
    html = models.TextField()

    def save(self, *args, **kwargs) -> None:
        html = markdown.markdown(
            self.body,
            output_format="html",
            extensions=settings.MARKDOWN_EXTENSIONS,
            extension_configs=settings.MARKDOWN_EXTENSIONS_CONFIG,
        )
        self.html = re.sub(r"\n\n", "\n", html)
        super().save(*args, **kwargs)

    def get_absolute_url(self) -> str:
        return reverse("now")

    def __str__(self) -> str:
        return self.created.isoformat()

    class Meta:
        verbose_name_plural = "now"
        ordering = ["-created"]

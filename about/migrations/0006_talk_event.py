# Generated by Django 5.1 on 2024-10-13 11:30
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("about", "0005_alter_now_options"),
    ]

    operations = [
        migrations.AddField(
            model_name="talk",
            name="event",
            field=models.CharField(default="", max_length=250),
        ),
    ]

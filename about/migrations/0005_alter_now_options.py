# Generated by Django 5.0.4 on 2024-05-31 18:55
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("about", "0004_now"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="now",
            options={"ordering": ["-created"], "verbose_name_plural": "now"},
        ),
    ]

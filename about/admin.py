from django.contrib import admin

from about import models


@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    fields = (
        "title",
        "link",
        "href",
        "description",
    )

    list_display = (
        "title",
        "short_description",
        "link",
        "href",
    )

    def short_description(self, obj) -> str:
        return f"{obj.description[:50]}..."


@admin.register(models.Talk)
class TalkAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "date",
        "location",
        "abstract",
    )


@admin.register(models.Biography)
class BiographyAdmin(admin.ModelAdmin):
    list_display = (
        "version",
        "date",
        "short_body",
    )
    fields = ("date", "version", "body")
    readonly_fields = ("date", "version")

    def version(self, obj: models.Biography) -> str:
        return f"v{obj.id}"

    def short_body(self, obj: models.Biography) -> str:
        return obj.body[:25]


@admin.register(models.Now)
class NowAdmin(admin.ModelAdmin):
    list_display = (
        "created",
        "body",
    )
    fields = (
        "created",
        "body",
    )
    readonly_fields = ("created",)

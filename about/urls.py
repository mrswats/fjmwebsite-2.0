from django.urls import path
from django.views.generic import TemplateView

from about import views


urlpatterns = [
    path("me/", views.about_me, name="about-me"),
    path("bots/", TemplateView.as_view(template_name="about/bots.html"), name="bots"),
    path("projects/", views.projects, name="projects"),
    path("talks/", views.talks, name="talks"),
    path("now/", views.now, name="now"),
]

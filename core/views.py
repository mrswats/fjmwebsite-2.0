import importlib.metadata as im

from django import shortcuts
from django.conf import settings
from django.http import FileResponse
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.cache import cache_control
from django.views.decorators.cache import never_cache
from django.views.decorators.http import require_GET

SOCIALS = ["github", "gitlab", "bluesky"]


@require_GET
@cache_control(max_age=60 * 60 * 24, immutable=True, public=True)
def favicon(request: HttpRequest) -> HttpResponse:
    favicon_file = (settings.STATIC_ROOT / "logo_fjm.png").open("rb")
    return FileResponse(favicon_file)


def landing(request: HttpRequest) -> HttpResponse:
    return shortcuts.render(request, "home.html", {"socials": SOCIALS})


@never_cache
def status(request: HttpRequest) -> JsonResponse:
    return JsonResponse(data={"status": "healthy!"})


def version(request: HttpRequest) -> JsonResponse:
    return JsonResponse(data={"version": im.version("fjmwebsite-2.0")})


def redirect_til_to_blog(
    request: HttpRequest, year: int, month: int, day: int, slug: str
) -> HttpResponse:
    return shortcuts.redirect("post", year=year, month=month, slug=slug, permanent=True)

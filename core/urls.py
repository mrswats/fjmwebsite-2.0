from django.urls import path
from django.views.generic import RedirectView

from core import views

urlpatterns = [
    path("", views.landing, name="home"),
    path("favicon.ico", views.favicon, name="favicon"),
    path("status/", views.status, name="status"),
    path("_version/", views.version, name="version"),
    path("til/", RedirectView.as_view(url="/blog/?t=TIL"), name="til"),
    path(
        "til/<int:year>/<int:month>/<int:day>/<slug:slug>/",
        views.redirect_til_to_blog,
        name="til-detail",
    ),
]

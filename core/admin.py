from django.contrib import admin
from django.contrib.auth import models as auth_models

from core import models

admin.site.unregister(auth_models.Group)


@admin.register(models.FjmwebsiteUser)
class UserAdmin(admin.ModelAdmin):
    search_fields = ("first_name", "last_name", "username")
    list_display = (
        "first_name",
        "last_name",
        "username",
        "email",
        "date_joined",
    )

from django.contrib.auth.models import AbstractUser
from django.db import models


class FjmwebsiteUser(AbstractUser):
    twitter_handle = models.CharField(max_length=50, blank=True, null=False, default="")

from collections.abc import Callable

import secure
from django.http import HttpRequest
from django.http import HttpResponse

secure_headers = secure.Secure.with_default_headers()


def set_secure_headers(get_response: Callable) -> Callable:
    def middleware(request: HttpRequest) -> HttpResponse:
        response = get_response(request)

        if request.path != "/favicon.ico":
            secure_headers.set_headers(response)

        return response

    return middleware
